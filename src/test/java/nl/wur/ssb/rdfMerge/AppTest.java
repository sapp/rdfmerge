package nl.wur.ssb.rdfMerge;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public void testApp() throws Exception {
    String[] args = {"-input",
        "./src/test/resources/file1.hdt,./src/test/resources/file2.hdt,./src/test/resources/file1.hdt",
        "-output", "./src/test/resources/merged.hdt"};
    App.main(args);
  }
}
