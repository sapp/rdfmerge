package nl.wur.ssb.rdfMerge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.exceptions.ParserException;
import org.rdfhdt.hdt.options.HDTSpecification;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.data.Domain;

public class App {
  private static CommandLine arguments;
  public static String jarPath;
  public static RDFSimpleCon sappRDFsource;
  public static Domain domain;

  public static void main(String[] args) throws Exception {
    Logger.getRootLogger().setLevel(Level.OFF);
    arguments = CommandParser.argsValidator(args);
    merge();
  }

  private static void merge() throws IOException, ParserException {
    HDT hdt = new HDT();
    List<File> ntFiles = new ArrayList<File>();

    for (String hdtfile : arguments.getOptionValue("input").split(",")) {
      File tmpFile = File.createTempFile("hdt", "nt");
      hdt.hdt2rdf(hdtfile, tmpFile.getAbsolutePath());
      ntFiles.add(tmpFile);
    }
    File mergedFile = File.createTempFile("hdt", "nt");
    MergerFiles.mergeFiles(ntFiles, mergedFile);
    String baseURI = "http://csb.wur.nl/genome/";
    String inputType = "ntriples";
    String HDT = arguments.getOptionValue("output");
    org.rdfhdt.hdt.hdt.HDT hdtManager =
        org.rdfhdt.hdt.hdt.HDTManager.generateHDT(mergedFile.getAbsolutePath(), baseURI,
            RDFNotation.parse(inputType), new HDTSpecification(), null);
    System.out.println("HDTSAVE: " + HDT);
    hdtManager.saveToHDT(HDT, null);
  }

}
